<?php

/**
 * 
 * @author Ahsan Bilal <ahsan.bilal@nxb.com.pk>
 * 
 * This class can be use for custom error notices. 
 * 
 * The notices should have the following types
 * @success
 * @error
 * 
 */
class MyPlugin_Notice {

    var $notice_type = array('success', 'error');

    function __construct() {
        
    }

    public static function getInstance() {
        $class = new MyPlugin_Notice();
        return $class;
    }

    /**
     * 
     * This method saves the notification in Session. 
     * The Notication Session can save multiple notifications of multiple types.
     * 
     * @param str $notice_txt: Message Notice text
     * @param type $type: Message Notice type should be @success , @error
     */
    public function set_msg($notice_txt, $type = 'success') {
        $_SESSION['MyPlugin_notice'][] = array(
            'type' => (in_array($type, $this->notice_type) ? $type : 'success'),
            'txt' => $notice_txt
        );
    }

    /**
     * 
     * This method shows/return the notifications after the page redirection.
     * 
     * @param boolean $show: 
     *  if @true, all the notifications (html) display 
     *  through this method
     *  if @false, all the notifications (html) return.
     * 
     * @return string
     */
    public function get_msg($show = true) {
        $html = '';
        if (isset($_SESSION['MyPlugin_notice']) && !empty($_SESSION['MyPlugin_notice'])) {
            foreach ($_SESSION['MyPlugin_notice'] as $notice) {
                $html .= '<div class="MyPlugin_' . $notice['type'] . ' MyPlugin_notice">';
                $html .= $notice['txt'];
                $html .= '</div>';
            }
            unset($_SESSION['MyPlugin_notice']);
        }

        if ($show) {
            echo $html;
        } else {
            return $html;
        }
    }

}
