<?php
/**
 * 
 * @author Ahsan Bilal <ahsan.bilal@nxb.com.pk>
 * 
 * Sample @layout view.
 * The layout name should specify in controller with class property @layout.
 * 
 * @content: add the shortcode [content] where the view content needs to load.
 * 
 */
?>
<div class="MyPlugin_page">
    <h1><?php echo (isset($this->page_title) ? $this->page_title : 'Page Title'); ?></h1>
    <div class="MyPlugin_content">
        [content]    
    </div>
</div>


