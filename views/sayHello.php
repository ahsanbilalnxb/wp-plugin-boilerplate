<?php
/**
 * 
 * @author Ahsan Bilal <ahsan.bilal@nxb.com.pk>
 * 
 * Sample @view.
 * 
 */
?>

<div class = "tabs">

    <div class = "tab">
        <input type = "radio" id = "tab-1" name = "tab-group-1" checked>
        <label for = "tab-1">Posts (<?php echo isset($total_posts[0]->total_posts) ? $total_posts[0]->total_posts : '0'; ?>)</label>

        <div class = "content">
            <?php if (isset($posts) && !empty($posts)) { ?>
                <table class="imagetable">
                    <tr>
                        <th>#</th><th>Title</th><th>Author</th>
                    </tr>
                    <?php
                    $i = 0;
                    foreach ($posts as $post) {
                        ?>
                        <tr>
                            <td><?= ++$i; ?></td><td><?= $post->post_title; ?></td><td><?= get_userdata($post->post_author)->user_login; ?></td>
                        </tr>
                    <?php }
                    ?>
                </table>
            <?php }
            ?>
        </div>
    </div>

    <div class = "tab">
        <input type = "radio" id = "tab-2" name = "tab-group-1">
        <label for = "tab-2">Tab Two</label>

        <div class = "content">
            stuffs
        </div>
    </div>

    <div class = "tab">
        <input type = "radio" id = "tab-3" name = "tab-group-1">
        <label for = "tab-3">Tab Three</label>

        <div class = "content">
            stuff
        </div>
    </div>

</div>

