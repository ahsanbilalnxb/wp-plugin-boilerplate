<?php

class MyPlugin_hello_model extends MyPlugin_BaseModel {

    function __construct() {
        parent::construct();
    }

    function getTotalPosts() {
        $sql = "SELECT COUNT(ID) as total_posts FROM wp_posts";
        $results = $this->db->get_results($sql) or die(mysql_error());
        return $results;
    }

    function getPosts() {
        $args = array(
            'posts_per_page' => 27
        );
        $posts_array = get_posts($args);
        return $posts_array;
    }

}
