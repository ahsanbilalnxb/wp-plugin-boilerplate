<?php
/**
 * @author Ahsan Bilal <ahsan.bilal@nxb.com.pk>
 * 
 * @MyPlugin_actions class automatically loads all the @WP-ACTIONS present in
 * @load method.
 * 
 */
class MyPlugin_actions {

    /**
     * all the actions that need to load 
     */
    public function load() {

        add_action('admin_menu', array(MyPlugin_Admin::getInstance(), 'load_menu'));

        if (isset($_GET['page']) && in_array($_GET['page'], array('myplugin', 'sayHello'))) {
            add_action('admin_enqueue_scripts', array(MyPlugin_Admin, 'load_scripts'));
        }
        return true;
    }

}
