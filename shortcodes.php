<?php

/**
 * To load the shortcodes of plugin
 */
class MyPlugin_shortcodes {

    function load() {
        
        add_shortcode('sayhello', array(MyPlugin_Hello::getInstance(), 'say_Hello'));
        
    }

}
