<?php

/**
 * Plugin Name: MyPlugin
 * Plugin URI:  http://wpquantum.com
 * Description: MyPlugin Description.
 * Author:      WP Quantum
 * Author URI:  http://wpquantum.com
 * Version:     2.5.4
 * Text Domain: bbpress
 * Domain Path: /languages/
 */

/**
 * @MyPlugin_VERSION : version of "MyPlugin" plugin.
 * @MyPlugin_PLUGINS_DIR : absolute path of wp-plugins directory
 * @MyPlugin_BASE_URL : Base URL of "MyPlugin"
 * @MyPlugin_ASSETS_URL : Assets URL to access CSS & JS Files of "MyPlugin".
 * @MyPlugin_BASE_DIR : absolute path of "MyPlugin" plugin.
 * @MyPlugin_SYSTEM_DIR : absolute path of "MyPlugin/system" directory.
 * @MyPlugin_LIBRARIES : absolute path of "MyPlugin/libraries" directory.
 * @MyPlugin_CONTROLLERS : absolute path of "MyPlugin/controllers" directory.
 * @MyPlugin_MODELS :  absolute path of "MyPlugin/models" directory.
 * @MyPlugin_VIEWS :   absolute path of "MyPlugin/views" directory.
 * @MyPlugin_LAYOUTS :   absolute path of "MyPlugin/layouts" directory.
 * @MyPlugin_THIRDPARTY_DIR : absolute path of "MyPlugin/thirdparty" directory.
 * @MyPlugin_THIRDPARTY_URL :   URL of "MyPlugin/models" directory.
 */

define('MyPlugin_VERSION', 1);
define('MyPlugin_PLUGINS_DIR', ABSPATH . 'wp-content/plugins/');
define('MyPlugin_BASE_URL', plugins_url('MyPlugin') . '/');
define('MyPlugin_ASSETS_URL', MyPlugin_BASE_URL . 'assets/');
define('MyPlugin_BASE_DIR', dirname(__FILE__) . '/');
define('MyPlugin_SYSTEM_DIR', dirname(__FILE__) . '/system/');
define('MyPlugin_LIBRARIES', MyPlugin_BASE_DIR . 'libraries/');
define('MyPlugin_CONTROLLERS', MyPlugin_BASE_DIR . 'controllers/');
define('MyPlugin_MODELS', MyPlugin_BASE_DIR . 'models/');
define('MyPlugin_VIEWS', MyPlugin_BASE_DIR . 'views/');
define('MyPlugin_LAYOUTS', MyPlugin_BASE_DIR . 'views/layouts/');
define('MyPlugin_THIRDPARTY_DIR', dirname(__FILE__) . '/thirdparty/');
define('MyPlugin_THIRDPARTY_URL', MyPlugin_BASE_URL . 'thirdparty/');

/**
 * Loading the plugin
 */
MYPlugin_load_file(MyPlugin_SYSTEM_DIR . 'MyPlugin_loader.php');
new MyPlugin_loader(true);


/**
 * For activation and deactivation of Plugin
 */
register_activation_hook(__FILE__, array(MyPlugin_Toggle::getInstance(), 'activate'));
register_deactivation_hook(__FILE__, array(MyPlugin_Toggle::getInstance(), 'deactivate'));

/**
 * 
 * @param type $path
 * @return boolean
 */
function MyPlugin_load_file($path) {
    if (file_exists($path)) {
        require_once($path);
    } else {
        return false;
    }
}

/**
 * 
 * @param type $str
 * @param type $exit
 */
function MyPlugin_debug($str, $exit = false) {
    echo "<pre>";
    print_r($str);
    echo "</pre>";
    if ($exit) {
        exit();
    }
}
