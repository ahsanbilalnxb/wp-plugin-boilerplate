<?php

/**
 * 
 * @author Ahsan Bilal <ahsan.bilal@nxb.com.pk>
 * 
 * This class can be use for admin panel events.
 *  It provides the feature to load the menus in admin panel.
 *  It load the @CSS & @JS files present in @assets directory.
 * 
 */
class MyPlugin_Admin extends MyPlugin_BaseController {

    function __construct() {
        parent::construct();
    }

    public static function getInstance() {
        $class = new MyPlugin_Admin();
        return $class;
    }

    /**
     * This method helps to load the menus in admin panel.
     */
    public function load_menu() {

        $title = apply_filters('MyPlugin_menu_title', "MyPlugin Option");
        add_menu_page($title, $title, 'manage_options', 'myplugin', array($this, 'sayHello'), MyPlugin_ASSETS_URL . 'images/wpq.png', 6);
        add_submenu_page('myplugin', 'sayHello', 'sayHello', 'manage_options', 'sayHello', array($this, 'hello_world'));
    }

    /**
     * This method helps to load the scripts in admin panel
     */
    public static function load_scripts() {
        wp_enqueue_script('MyPlugin_js', MyPlugin_ASSETS_URL . 'js/script.js');
        wp_enqueue_style('MyPlugin_css', MyPlugin_ASSETS_URL . 'css/style.css');
    }

    public function sayHello() {
        $hello_model = $this->model('MyPlugin_hello_model');

        $data = array(
            'total_posts' => $hello_model->getTotalPosts(),
            'posts' => $hello_model->getPosts(),
        );
        $this->layout = 'tabular';
        $this->page_title = 'Buddy :)';
        $this->view('sayHello', $data);
    }

    public function hello_world() {
        
    }

}
