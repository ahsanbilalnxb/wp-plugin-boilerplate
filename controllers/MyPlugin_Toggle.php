<?php

/**
 * 
 * @author Ahsan Bilal <ahsan.bilal@nxb.com.pk>
 * 
 * This class autmatically loads through the system and 
 * run the activation and deactivation script of MyPlugin.
 * 
 */
class MyPlugin_Toggle extends MyPlugin_BaseController {

    function __construct() {
        parent::construct();
    }

    public static function getInstance() {
        $class = new MyPlugin_Toggle();
        return $class;
    }

    /**
     * This method executes when the plugin activates
     * 
     * @return string
     */
    public function activate() {
        return "Helo I am activate now. :)";
    }

    /**
     * This method executes when the plugin deactivate
     * 
     * @return string
     */
    public function deactivate() {
        return "Helo I am deactivate now. :(";
    }

}
