<?php

class MyPlugin_Hello extends MyPlugin_BaseController {

    function __construct() {
        parent::construct();
    }

    public static function getInstance() {
        $class = new MyPlugin_hello();
        return $class;
    }

    public function say_Hello() {

        $hello_model = $this->model('MyPlugin_hello_model');

        $data = array(
            'total_posts' => $hello_model->getTotalPosts(),
            'posts' => $hello_model->getPosts(),
        );
        $this->layout = 'tabular';
        $this->view('sayHello', $data);
    }

}
