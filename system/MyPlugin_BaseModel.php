<?php

/**
 * 
 * @author Ahsan Bilal <ahsan.bilal@nxb.com.pk>
 * 
 * To provide the general db functions which the WP doesnot provide by default
 * 
 * @db: to provide the global variable of @wpdb
 * 
 */
abstract class MyPlugin_BaseModel {

    var $db;

    function construct() {
        global $wpdb;
        $this->db = $wpdb;
    }

}
