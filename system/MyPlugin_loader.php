<?php

/**
 * @author Ahsan Bilal <ahsan.bilal@nxb.com.pk>
 * 
 * This is the loader Class to initiate the plugin 
 * and includes the existing 
 * @controllers / *.php
 * @libraries / *.php
 * @actions.php wp-actions from @actions class (MyPlugin/actions.php)
 * wp-filters from @filters class (MyPlugin/filters.php)
 * wp-shortcodes from @shortcode class (MyPlugin/shortcodes.php)
 * 
 */
class MyPlugin_loader {

    /**
     * The constructor loads controller, libraries, actions, filters and 
     * shortcodes in there desired sequence.
     * 
     * @param type $load_all
     */
    public function __construct($load_all = false) {

        if ($load_all) {
            $this->controllers();
            $this->libraries();
            $this->actions();
            $this->filters();
            $this->shortcodes();
        }
    }

    /**
     * This method includes all the controller files present in 
     * MyPlugin/controllers
     */
    private function controllers() {
        MyPlugin_load_file(MyPlugin_SYSTEM_DIR . 'MyPlugin_BaseController.php');
        $files = glob(MyPlugin_CONTROLLERS . "/*.php");
        if (is_array($files) && !empty($files)) {
            foreach (glob(MyPlugin_CONTROLLERS . "/*.php") as $filename) {
                require_once($filename);
            }
        } else {
            return false;
        }
    }

    /**
     * This method includes all the library files present in 
     * MyPlugin/libraries directory.
     * 
     */
    private function libraries() {
        $files = glob(MyPlugin_LIBRARIES . "/*.php");
        if (is_array($files) && !empty($files)) {
            foreach (glob(MyPlugin_LIBRARIES . "/*.php") as $filename) {
                require_once($filename);
            }
        } else {
            return false;
        }
    }

    /**
     * To load the action class,
     * It automatically loads the method 'load()' from shortcode class
     * 
     */
    private function actions() {
        if (file_exists(MyPlugin_BASE_DIR . 'actions.php')) {
            require_once(MyPlugin_BASE_DIR . 'actions.php');
            if (class_exists('MyPlugin_actions')) {
                $action = new MyPlugin_actions();
                $action->load();
            }
        }
    }

    /**
     * To load the filters class.
     * It automatically loads the method 'load()' from Filters class
     */
    private function filters() {
        if (file_exists(MyPlugin_BASE_DIR . 'filters.php')) {
            require_once(MyPlugin_BASE_DIR . 'filters.php');
            if (class_exists('MyPlugin_filters')) {
                $filter = new MyPlugin_filters();
                $filter->load();
            }
        }
    }

    /**
     * To load the shortcode class.
     * It automatically loads the method 'load()' from shortcode class
     */
    private function shortcodes() {
        if (file_exists(MyPlugin_BASE_DIR . 'shortcodes.php')) {
            require_once(MyPlugin_BASE_DIR . 'shortcodes.php');
            if (class_exists('MyPlugin_shortcodes')) {
                $shortcodes = new MyPlugin_shortcodes();
                $shortcodes->load();
            }
        }
    }

}
