<?php

/**
 * @author Ahsan Bilal <ahsan.bilal@nxb.com.pk>
 * 
 * 
 * This is the Base Controller which will support to load the 
 * @model
 * @views
 * @layout: views > layout
 */
abstract class MyPlugin_BaseController {

    var $load;
    var $layout = '';

    function construct() {
        
    }

    /**
     * This method 
     *  - includes the model file and 
     *  - creates its class instance 
     * 
     * To get the model class Instance the model file name and class name 
     * should be same.
     * 
     * 
     * @param string $path: name of model file present in models directory.
     * @return class
     */
    public function model($path = '') {

        MyPlugin_load_file(MyPlugin_SYSTEM_DIR . 'MyPlugin_BaseModel.php');

        $file_path = MyPlugin_MODELS . $path . '.php';
        if (file_exists($file_path)) {
            MyPlugin_load_file($file_path);
            $path_ar = explode('/', $path);
            $class_id = end($path_ar);
            if (class_exists($class_id)) {
                $class = new $class_id();
                return $class;
            } else {
                echo "Model class doesnot exists";
            }
        } else {
            echo "Model path " . $path . " doesnot exists.";
        }
    }

    /**
     * This method 
     *  - includes the library file and 
     *  - creates its class instance 
     * 
     * To get the library class Instance the model file name and class name 
     * should be same.
     * 
     * 
     * @param string $path: name of library file present in models directory.
     * @return class
     */
    public function library($path = '') {

        $file_path = MyPlugin_LIBRARIES . $path . '.php';
        if (file_exists($file_path)) {
            MyPlugin_load_file($file_path);
            $path_ar = explode('/', $path);
            $class_id = end($path_ar);
            if (class_exists($class_id)) {
                $class = new $class_id();
                return $class;
            } else {
                echo "Library class doesnot exists";
            }
        } else {
            echo "Library path " . $path . " doesnot exists.";
        }
    }

    /**
     * 
     * This method 
     * includes the view the file and 
     * also render the view in layout if layout define in @layout
     * 
     * @param string $path: name of view file present in views directory without
     *  .php extension. 
     * 
     * @param array $data:  (optional) data need to render in view file or 
     *  layout file. In view the data retrieves through array @keys.
     * 
     * @param boolean $show_content: (optional) 
     *  If @true, method displays the content through this method. 
     *  If @false, method returns the content html .
     */
    public function view($path, $data = array(), $show_content = true) {
        if (isset($path) && !empty($path)) {
            $path = MyPlugin_VIEWS . $path . '.php';
            if (file_exists($path)) {

                if (isset($data) && is_array($data) && !empty($data)) {
                    extract($data);
                }

                ob_start();
                require($path);
                $content_data = ob_get_clean();

                if (isset($this->layout) && $this->layout != '') {
                    $layout_path = MyPlugin_LAYOUTS . $this->layout . '.php';
                    if (file_exists($layout_path)) {
                        ob_start();
                        require($layout_path);
                        $layout_data = ob_get_clean();

                        $content_data = str_replace('[content]', $content_data, $layout_data);
                    }
                }

                if (isset($show_content) && $show_content === false) {
                    return $content_data;
                } else {
                    echo $content_data;
                }
            } else {
                echo "View doesnot exists.";
            }
        } else {
            echo "View Path doesnot exists.";
        }
    }

}
